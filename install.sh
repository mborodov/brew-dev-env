#!/bin/bash

case "$1" in
"php52")
  PHP_VERSION='52' ;;
"php53")
  PHP_VERSION='53' ;;
"php54")
  PHP_VERSION='54' ;;
"php55")
  PHP_VERSION='55' ;;
*)
  PHP_VERSION='55' ;;
"php56")
  PHP_VERSION='56' ;;
*)
  PHP_VERSION='56' ;;
esac

echo "Your choice is PHP${PHP_VERSION}!"
echo "----- ✄ -----------------------"

echo '✩✩✩✩ Add Repositories ✩✩✩✩'
brew tap homebrew/dupes
brew tap homebrew/versions
brew tap josegonzalez/homebrew-php
brew update

echo '✩✩✩✩ MYSQL ✩✩✩✩'
brew install mysql
mysql_install_db --verbose --user=`whoami` --basedir="$(brew --prefix mysql)" --datadir=/usr/local/var/mysql --tmpdir=/tmp

echo '✩✩✩✩ NGINX ✩✩✩✩'
brew install nginx

echo '-> Download configs'
mkdir /usr/local/etc/nginx/{common,sites-available,sites-enabled}

curl -Lo /usr/local/etc/nginx/nginx.conf https://bitbucket.org/mborodov/brew-dev-env/raw/e8ce92421ac144440589e6f2818791a88d51eddb/conf/nginx/nginx.conf

curl -Lo /usr/local/etc/nginx/common/drupal https://bitbucket.org/mborodov/brew-dev-env/raw/e8ce92421ac144440589e6f2818791a88d51eddb/conf/nginx/common/magento

# Download Virtual Hosts.
curl -Lo /usr/local/etc/nginx/sites-available/default https://bitbucket.org/mborodov/brew-dev-env/raw/e8ce92421ac144440589e6f2818791a88d51eddb/conf/nginx/sites-available/default
curl -Lo /usr/local/etc/nginx/sites-available/drupal.local https://bitbucket.org/mborodov/brew-dev-env/raw/e8ce92421ac144440589e6f2818791a88d51eddb/conf/nginx/sites-available/magento.dev

ln -s /usr/local/etc/nginx/sites-available/default /usr/local/etc/nginx/sites-enabled/default

# Create folder for logs.
rm -rf /usr/local/var/log/{fpm,nginx}
mkdir -p /usr/local/var/log/{fpm,nginx}

echo '✩✩✩✩ PHP + FPM ✩✩✩✩'
brew install freetype jpeg libpng gd
brew install php${PHP_VERSION} --without-apache --with-mysql --with-fpm --without-snmp --disable-opcache
brew link --overwrite php${PHP_VERSION}

echo '✩✩✩✩ APC ✩✩✩✩'
brew install php${PHP_VERSION}-apcu

echo '✩✩✩✩ Memcached ✩✩✩✩'
brew install php${PHP_VERSION}-memcached

echo '✩✩✩✩ Xdebug ✩✩✩✩'
brew install php${PHP_VERSION}-xdebug

case "${PHP_VERSION}" in
"52")
  DOT_VERSION='5.2' ;;
"53")
  DOT_VERSION='5.3' ;;
"54")
  DOT_VERSION='5.4' ;;
"55")
  DOT_VERSION='5.5' ;;
"56")
  DOT_VERSION='5.6' ;;
esac

echo 'xdebug.remote_enable=On' >>  /usr/local/etc/php/${DOT_VERSION}/conf.d/ext-xdebug.ini
echo 'xdebug.remote_host="localhost"' >>  /usr/local/etc/php/${DOT_VERSION}/conf.d/ext-xdebug.ini
echo 'xdebug.remote_port=9002' >>  /usr/local/etc/php/${DOT_VERSION}/conf.d/ext-xdebug.ini
echo 'xdebug.remote_handler="dbgp"' >>  /usr/local/etc/php/${DOT_VERSION}/conf.d/ext-xdebug.ini

echo '✩✩✩✩ dnsmasq ✩✩✩✩'
brew install dnsmasq
echo 'address=/.dev/127.0.0.1' >>  /usr/local/etc/dnsmasq.conf

echo '✩✩✩✩ Install complete ✩✩✩✩'